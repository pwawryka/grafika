Ground = {
	texture: null,
	material: null,
	geometry: null,
	ground: null,

	init: function (scene) {
		texture = THREE.ImageUtils.loadTexture('textures/ground.jpg', undefined, function() {

			_texture = THREE.ImageUtils.loadTexture('textures/floor-wood.jpg')

			// create THREE js object
			this.geometry = new THREE.PlaneGeometry(1000000, 1000000, 100, 100);
			this.material = new THREE.MeshBasicMaterial({ map: _texture, side: THREE.DoubleSide });
			this.ground = new THREE.Mesh(this.geometry, this.material);

			imageData = getImageData(texture.image);

			for(var i = 0; i < this.geometry.vertices.length; i++){
				vertex = this.geometry.vertices[i]
				var x = Math.round((vertex.x + (1000000 / 2)) / (1000000 / 2048));
				var y = Math.round((vertex.y + (1000000 / 2)) / (1000000 / 2048));
				var pixel = getPixel(imageData, x, 2048 - y);

				vertex.z = 300 * (pixel.r - pixel.b - pixel.g);
			}

			scene.add(this.ground);
		});
		
		this.texture = texture;
	},
}

function getImageData(image) {
	var canvas = document.createElement('canvas');
	canvas.width = image.width;
	canvas.height = image.height;

	var context = canvas.getContext('2d');
	context.drawImage(image, 0, 0);

	return context.getImageData(0, 0, image.width, image.height);
}

function getPixel(imagedata, x, y) {
	var position = (x + imagedata.width * y) * 4;
	return {
		r: imagedata.data[position],
		g: imagedata.data[position + 1],
		b: imagedata.data[position + 2],
		a: imagedata.data[position + 3]
	};
}