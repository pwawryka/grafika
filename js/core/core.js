Core = {
	container: null,
	renderer: null,
	scene: null,
	camera: null,
	controls: null,
	keyboard: null
}

Core.init = function() {
	// scene
	Core.scene = new THREE.Scene();

	// camera
	var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
	var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
	Core.camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
	Core.camera.position.set(0,-20,5);
	//Core.camera.lookAt(Core.scene.position);

	// renderer
	Core.renderer = Core.getRenderer();
	Core.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	Core.container = document.getElementById('ThreeJS');
	Core.container.appendChild(Core.renderer.domElement);


	// events
	THREEx.WindowResize(Core.renderer, Core.camera);
	THREEx.FullScreen.bindKey({charCode : 'm'.charCodeAt(0)});

	// controls
	Core.controls = new THREE.OrbitControls(Core.camera, Core.renderer.domElement);

	// keyboard
	Core.keyboard = new THREEx.KeyboardState();

	// light
	var light = new THREE.PointLight(0xffffff);
	light.position.set(0,250,0);
	Core.scene.add(light);

	// ground
	Ground.init(Core.scene);

	// plane
	Plane.init(Core.scene, Core.camera);
}

Core.run = function() {
	Core.animate();
}

Core.animate = function() {
	requestAnimationFrame(Core.animate);
	Core.render();		
	Core.update();
}

Core.update = function() {

	if(Core.keyboard.pressed('w')) {
		Plane.move();
	}

	Core.controls.update();
}

Core.render = function() {
	Core.renderer.render(Core.scene, Core.camera);
}

Core.getRenderer = function() {
	var renderer = null;
	if (Detector.webgl)
		renderer = new THREE.WebGLRenderer( {antialias:true} );
	else
		renderer = new THREE.CanvasRenderer(); 

	return renderer;
}