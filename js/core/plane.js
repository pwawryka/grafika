Plane = {
	loader: null,
	plane: null,
	light: null
}

Plane.init = function(scene, camera) {
	Plane.light = new THREE.PointLight(0xffffff);
	Plane.light.position.set(0, 10, 10);

	Plane.loader = new THREE.OBJMTLLoader();
	Plane.loader.addEventListener('load', function (event) {

		Plane.plane = event.content;

		Plane.plane.scale.set(100, 100, 100);
		Core.scene.add(Plane.plane);

		Plane.plane.camera = camera;
		Plane.plane.add(camera);
		Plane.plane.add(Plane.light);

		Plane.plane.rotation.x = 0;
		Plane.plane.rotation.y = 0;
		Plane.plane.rotation.z = 0;
	});

	Plane.loader.load('models/FA-22_Raptor.obj', 'models/FA-22_Raptor.mtl', {side: THREE.DoubleSide});
	scene.add(Plane.plane);
}

Plane.move = function () {
	Plane.plane.position.y += 100;
}